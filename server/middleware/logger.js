import winston from 'winston';
import morgan from 'morgan';

const colors = {
  error: 'red',
  warn: 'cyan',
  info: 'blue',
  http: 'green',
  debug: 'yellow',
};

winston.addColors(colors);

const format = winston.format.combine(
  winston.format.timestamp({ format: 'YYYY-MM-DD HH:mm:ss' }),
  winston.format.splat(),
);

const logFormat = winston.format.combine(
  winston.format.colorize({ all: true }),
  winston.format.printf(
    (info) => `${info.timestamp} [${info.level}] : ${info.message}`,
  ),
);

const LEVEL = Symbol.for('level');
const filterOnly = (level) => {
  return winston.format((info) => {
    if (info[LEVEL] === level) {
      return info;
    }
    return null;
  })();
};

const Logger = winston.createLogger({
  level: 'debug',
  format,
  transports: [
    new winston.transports.Console({
      format: logFormat,
    }),
    new winston.transports.File({
      filename: 'logs/error.log',
      level: 'debug',
      maxsize: 5242880, // 5 MB
      maxFiles: 1,
      format: winston.format.combine(
        winston.format.json(),
        filterOnly('debug'),
      ),
    }),
    new winston.transports.File({
      filename: 'logs/combined.log',
      maxsize: 5242880, // 5 MB
      maxFiles: 1,
      format: winston.format.combine(winston.format.json()),
    }),
  ],
  exitOnError: false,
});

const stream = {
  write: (message) => Logger.http(message.trim()),
};

const skip = () => {
  return false;
};

morgan.token('pathname', (req) => req.path);
morgan.token('queries', (req) => JSON.stringify(req.query).replace(/\\/g, ''));

const morganMiddleware = morgan(
  (tokens, req, res) =>
    [
      tokens.method(req, res),
      tokens.pathname(req),
      '\n',
      tokens.queries(req),
      '\n',
      tokens.status(req, res),
      tokens.res(req, res, 'content-length'),
      '-',
      tokens['response-time'](req, res),
      'ms',
    ].join(' '),
  { stream, skip },
);

export { Logger, morganMiddleware };
