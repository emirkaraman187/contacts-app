import { Errors } from '../utils/error.js';
import { Logger } from './logger.js';

const fallthroughErrorHandler = async (err, req, res, next) => {
  
  Logger.debug(JSON.stringify(err, null, 2));

  // Specific validation error handler
  if (err?.statusCode) res.status(err?.statusCode).send(err);
  // Integrity constraint error handler
  else if (err.errorNum === 2292)
    res.status(Errors.DELETE_FORBIDDEN.statusCode).send({
      ...Errors.DELETE_FORBIDDEN,
      details: {
        reason: 'Nije moguće obrisati vrijednost jer postoje zavisni redovi',
      },
    });
  // Generic error handler
  else
    res.status(err.statusCode || 500).send({
      error: {
        type: err?.type || 'internal_server_error',
        message: err?.message || 'Unexpected internal server error.',
        details: err?.details,
        sentry: res?.sentry,
      },
    });
};

export default fallthroughErrorHandler;
