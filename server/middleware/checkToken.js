import jwt from "jsonwebtoken";
import pool from "../database/index.js"
import dotenv from 'dotenv'
dotenv.config()

const checkToken = (req, res, next) => {
    let token = req.get("Authorization");
    if (token) {
      token = token.slice(7);
      jwt.verify(token, process.env.JWT_SECRET, async (err, decoded) => {
        if (err) {
          req.validToken = false;
          next();
        } else {
          req.decoded = decoded;
          if(decoded.data.email){
            const client = await pool.connect()
            const results = await client.query(
              `select * from users where email = $1`,[decoded.data.email]
            )
            client.release();
            if(results.rows.length === 1){
              console.log("You are logged in!")
              req.validToken = true;
              next();
            }else{
              req.validToken = false;
              next();
            }
          }else{
            req.validToken = false;
            next();
          }
        }
      });
    } else {
      req.validToken = false;
      next();
    }
}

export default checkToken;
