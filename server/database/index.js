import dotenv from 'dotenv'
import pkg from 'pg'
const { Pool } = pkg;
dotenv.config()

const pool = new Pool({
    user: process.env.DB_USER,
    password: process.env.DB_PASS,
    host: process.env.DB_HOST,
    port: process.env.DB_PORT,
    database: process.env.MYSQL_DB,
});
  
export default pool;