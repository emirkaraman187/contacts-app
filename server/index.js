import express from 'express';
import dotenv from 'dotenv'
import cors from 'cors';
import helmet from 'helmet';
import bodyParser from 'body-parser';
import compression from 'compression';
import rateLimit from 'express-rate-limit';
import fallthroughErrorHandler from './middleware/falltroughErrorHandler.js';
import { morganMiddleware } from './middleware/logger.js';

import contactRouter from './api/contact/index.js';
import authRouter from './api/auth/index.js';

dotenv.config()
const app = express();

const limiter = rateLimit({ windowMs: 5 * 60 * 1000, max: 500 });

// middleware
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cors({ 
    credentials: true, 
    origin: `http://localhost:${process.env.CLIENT_PORT}`
}));
app.enable('trust proxy');
app.use(helmet());
app.use(bodyParser.json({ limit: '10mb' }));
app.use(compression());
app.use(limiter);
app.use(morganMiddleware);

// routes
app.use('/api/contact', contactRouter);
app.use('/api/auth', authRouter);

app.use(fallthroughErrorHandler);

app.listen(process.env.SERVER_PORT, () => {
    console.log(`Server has started on port: ${process.env.SERVER_PORT}`)
})