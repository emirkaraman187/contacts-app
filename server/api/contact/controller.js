import * as service from './service.js';

export const createContact = async (req, res, next) => {
  try {
    if(req.validToken === false)
      return res.status(200).send({ invalidToken: true });
    const result = await service.createContact({ ...req?.params, ...req?.body });
    return res.status(200).send(result);
  } catch (error) {
    return next(error);
  }
};

export const getContactById = async (req, res, next) => {
    try {
      const result = await service.getContactById(req?.params);
      return res.status(200).send(result);
    } catch (error) {
      return next(error);
    }
};

export const getAllContacts = async (req, res, next) => {
    try {
      if(req.validToken === false)
        return res.status(200).send({ invalidToken: true });
      const result = await service.getAllContacts({...req?.params});
      return res.status(200).send(result);
    } catch (error) {
      return next(error);
    }
};

export const editContact = async (req, res, next) => {
    try {
      if(req.validToken === false)
        return res.status(200).send({ invalidToken: true });
      const result = await service.editContact({ ...req?.params, ...req?.body });
      return res.status(200).send(result);
    } catch (error) {
      return next(error);
    }
};

export const deleteContact = async (req, res, next) => {
    try {
      if(req.validToken === false)
        return res.status(200).send({ invalidToken: true });
      const result = await service.deleteContact(req?.params);
      return res.status(200).send(result);
    } catch (error) {
      return next(error);
    }
};


  
