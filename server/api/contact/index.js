import express from "express";
import checkToken from "../../middleware/checkToken.js";
import * as controller from "./controller.js";

const contactRouter = express.Router();
const api = (contactRouter) => {
    contactRouter.post("/createContact/:idUser",checkToken, controller.createContact);
    contactRouter.get("/getContactById/:id", controller.getContactById);
    contactRouter.get("/getAllContacts/:idUser/:filters",checkToken, controller.getAllContacts);
    contactRouter.put("/editContact/:id",checkToken, controller.editContact);
    contactRouter.delete("/deleteContact/:id",checkToken, controller.deleteContact);
};
api(contactRouter);
export default contactRouter;