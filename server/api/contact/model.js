import pool from "../../database/index.js"
import { Errors, APIError } from '../../utils/error.js';

export const createContact = async (idUser, contact) => {
    const { firstName, lastName, email, phoneNumber } = contact;

    const client = await pool.connect();

    const result = await client.query(
        `insert into contacts(first_name, last_name, email, phone_number, id_user, time_created) 
        values($1,$2,$3,$4,$5,current_timestamp) returning id`, [ firstName, lastName, email, phoneNumber, idUser]
    )
    client.release();
    if(result){
        return result.rows[0].id;
    }else{
        throw new APIError(Errors.INTERNAL_SERVER_ERROR, {
            contact,
            reason: `Error during the creation of a new contact.`,
        });
    }
};

export const getContactById = async (id) => {
    const client = await pool.connect()

    const result = await client.query(
        `select 
            id "id", 
            first_name "firstName", 
            last_name "lastName", 
            email "email", 
            phone_number "phoneNumber"
        from
            contacts where id = $1`, [ id ]
    );
    client.release()
    if(result){
        return result.rows[0];
    }else{
        const errorObj = { id }
        throw new APIError(Errors.INTERNAL_SERVER_ERROR, {
            errorObj,
            reason: `Error getting the contact with an id: ${id}.`,
        });
    }
};

export const getAllContacts = async (idUser) => {
    const client = await pool.connect()

    const result = await client.query(
        `select 
            id "id", 
            first_name "firstName", 
            last_name "lastName", 
            email "email", 
            phone_number "phoneNumber"
        from contacts
            where id_user = $1 order by id desc`, 
            [idUser]
    );
    client.release()
    if(result){
        return {
            contacts: result.rows,
        };
    }else{
        const errorObj = { idUser }
        throw new APIError(Errors.INTERNAL_SERVER_ERROR, {
            errorObj,
            reason: `Error getting the contacts for a user with an id: ${idUser}.`,
        });
    }
};

export const editContact = async (id, contact) => {
    const { firstName, lastName, email, phoneNumber } = contact;

    const client = await pool.connect()

    const result = await client.query(
        `update contacts set first_name = $1, last_name = $2, email = $3, 
        phone_number = $4, time_modiffied = current_timestamp 
        where id = $5`, [ firstName, lastName, email, phoneNumber, id]
    )
    client.release()
    if(result){
        return id;
    }else{
        const errorObj = { id };
        throw new APIError(Errors.INTERNAL_SERVER_ERROR, {
            errorObj,
            reason: `Error while updating the contact with an id: ${id}.`,
        });
    }
};

export const deleteContact = async (id) => {
    const client = await pool.connect()

    const result = await client.query(
        `delete from contacts where id = $1`, [ id ]
    )
    client.release();
    if(result){
        return { id };
    }else{
        const errorObj = { id };
        throw new APIError(Errors.INTERNAL_SERVER_ERROR, {
            errorObj,
            reason: `Error during deletion for the contact with an id: ${id}.`,
        });
    }
};