import * as model from './model.js';
import { Errors, APIError } from '../../utils/error.js';

export const createContact = async ({ idUser, contact }) => {
  if (!idUser) throw new APIError(Errors.MISSING_ARGUMENTS, { idUser });
  if (!/^[1-9][0-9]*$/.test(idUser))
  throw new APIError(Errors.BAD_REQUEST, {
    idUser,
    reason: `Argument "idUser" needs to be a positive integer.`,
  });

  const newContactId = await model.createContact(idUser, contact);
  return model.getContactById(newContactId);
};

export const getContactById = async ({ id }) => {
  if (!id) throw new APIError(Errors.MISSING_ARGUMENTS, { id });
  if (!/^[1-9][0-9]*$/.test(id))
    throw new APIError(Errors.BAD_REQUEST, {
      id,
      reason: `Argument "id" needs to be a positive integer.`,
    });

  const contact = await model.getContactById(id);
  return contact;
};

export const getAllContacts = async ({ idUser }) => {
  if (!idUser) throw new APIError(Errors.MISSING_ARGUMENTS, { idUser });
  if (!/^[1-9][0-9]*$/.test(idUser))
  throw new APIError(Errors.BAD_REQUEST, {
    idUser,
    reason: `Argument "idUser" needs to be a positive integer.`,
  });

  const contacts = await model.getAllContacts(idUser);
  return contacts;
};

export const editContact = async ({ id, contact }) => {
  if (!id) throw new APIError(Errors.MISSING_ARGUMENTS, { id });
  if (!/^[1-9][0-9]*$/.test(id))
    throw new APIError(Errors.BAD_REQUEST, {
      id,
      reason: `Argument "id" needs to be a positive integer.`,
    });

  const contactId = await model.editContact(id, contact);
  return model.getContactById(contactId);
};

export const deleteContact = async ({ id }) => {
  if (!id) throw new APIError(Errors.MISSING_ARGUMENTS, { id });
  if (!/^[1-9][0-9]*$/.test(id))
    throw new APIError(Errors.BAD_REQUEST, {
      id,
      reason: `Argument "id" needs to be a positive integer.`,
    });

  const exists = model.getContactById(id);
  if(exists){
    await model.deleteContact(id);
    return exists;
  }
  else
    throw new APIError(Errors.NOT_FOUND, {
      id,
      reason: `Contact with ${id} does not exist.`,
    });
};