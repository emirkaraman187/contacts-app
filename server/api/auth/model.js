import pool from "../../database/index.js"
import { hashSync, genSaltSync, compare } from "bcrypt";
import jwt from 'jsonwebtoken';
import dotenv from 'dotenv'
import { Errors, APIError } from '../../utils/error.js';
import nodemailer from 'nodemailer';
dotenv.config();
const secret = process.env.JWT_SECRET;

const generateConfirmationCode = () => {
    const characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    let code = '';
    for (let i = 0; i < 25; i++) {
        code += characters[Math.floor(Math.random() * characters.length )];
    }
    return code;
}

export const register = async (user) => {
    const { firstName, lastName, email, password } = user;

    //check email if unique
    const emailRows = await checkEmail(email);
    if(emailRows < 1){  
        const client = await pool.connect()
        const salt = genSaltSync(10);
        const hashed_password = hashSync(password, salt);
        const confirmationCode = generateConfirmationCode();
        const result = await client.query(
            `insert into users(first_name, last_name, email, password, time_created, confirmation_code, is_active) 
            values($1,$2,$3,$4,current_timestamp,$5,false) returning id`, 
            [ firstName, lastName, email, hashed_password, confirmationCode]
        );
        client.release();
        if(result){    // user added to database, now send email verification
            const transport = nodemailer.createTransport({
                service: "Gmail",
                auth: {
                  user: process.env.EMAIL_USER,
                  pass: process.env.EMAIL_PASS,
                },
              });
            const mailOptions = {
                from: process.env.EMAIL_USER,
                to: email,
                subject: "Please confirm your account",
                html: `<h1>Email Confirmation</h1>
                    <h2>Hello ${firstName} ${lastName}</h2>
                    <p>Please confirm your email by clicking on the following link</p>
                    <a href=http://localhost:5000/api/auth/confirm/${confirmationCode}> Click here</a>
                    </div>`,
            }
            transport.sendMail(mailOptions, (error) => {
                if (error) {
                    console.log(error);
                    throw new APIError(Errors.INTERNAL_SERVER_ERROR, {
                        user,
                        reason: `Error during email sending for email verification.`,
                    });
                }
                return "Waiting for email verification.";
              });
        }else{
            throw new APIError(Errors.INTERNAL_SERVER_ERROR, {
                user,
                reason: `Error during the registration of a new user.`,
            });
        }
    }else{
        throw new APIError(Errors.INTERNAL_SERVER_ERROR, {
            user,
            reason: `Email already exists.`,
        });
    }
};

export const login = async (user) => {
    const { email, password } = user;

    const client = await pool.connect();

    const result = await client.query(
        `select 
        id "id",
        first_name "firstName",
        last_name "lastName", 
        email "email", 
        password "password",
        is_active "isActive"
    from
        users where email = $1`, [ email ]
    )
    client.release();
    if(result.rows.length === 1){   // check password match
        const match = await compare(password, result.rows[0].password);
        if(match) {   // user is correct, check if email is veriffied
            if(result.rows[0].isActive === true){    // user is veriffied
                let token = jwt.sign({
                    exp: Math.floor(Date.now() / 1000) + (60 * 60),
                    data: {
                        id: result.rows[0].id,
                        email,
                        firstName: result.rows[0].firstName,
                        lastName: result.rows[0].lastName,
                    }
                }, secret);    
                return {
                    token,
                    email,
                    id: result.rows[0].id,
                    firstName: result.rows[0].firstName,
                    lastName: result.rows[0].lastName,
                }
            }else{
                throw new APIError(Errors.INTERNAL_SERVER_ERROR, {
                    user,
                    reason: `Your email address has not been yet veriffied!`,
                });
            }  
        }else{
            throw new APIError(Errors.INTERNAL_SERVER_ERROR, {
                user,
                reason: `Password does not match!`,
            });
        }
    }else{
        throw new APIError(Errors.INTERNAL_SERVER_ERROR, {
            user,
            reason: `Email does not match!`,
        });
    }
};

const checkEmail = async (email) => {
    const client = await pool.connect();
    const result = await client.query(
        `select * from users where email = $1`, [ email ]
    )
    client.release();
    return result.rows.length;
}

export const verifyUser = async (confirmationCode) => {
    const client = await pool.connect();

    const result = await client.query(
        `select * from users where confirmation_code = $1`, [ confirmationCode ]
    )
    
    if(result.rows.length === 1){   // email is verified
        await client.query(
            `update users set is_active = true where confirmation_code = $1`, [ confirmationCode ]
        )
        console.log("Email activated!");
        client.release();
        return "<h2>Your email has been veriffied, you may now log into your account</h2>";
    }else{
        client.release();
        const errorObj = { confirmationCode }
        throw new APIError(Errors.INTERNAL_SERVER_ERROR, {
            errorObj,
            reason: `Email verification is not working propperly`,
        });
    }
};

