import * as service from './service.js';

export const register = async (req, res, next) => {
  try {
    const result = await service.register(req?.body);
    return res.status(200).send(result);
  } catch (error) {
    return next(error);
  }
};

export const login = async (req, res, next) => {
  try {
    const result = await service.login(req?.body);
    return res.status(200).send(result);
  } catch (error) {
    return next(error);
  }
};

export const verifyUser = async (req, res, next) => {
  try {
    const result = await service.verifyUser({...req?.params});
    return res.status(200).send(result);
  } catch (error) {
    return next(error);
  }
};




  
