import express from "express";
import * as controller from "./controller.js";

const authRouter = express.Router();
const api = (authRouter) => {
    authRouter.post("/register", controller.register);
    authRouter.post("/login", controller.login);
    authRouter.get("/confirm/:confirmationCode", controller.verifyUser);
};
api(authRouter);
export default authRouter;