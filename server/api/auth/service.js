import * as model from './model.js';

export const register = async ({ user }) => {
  const result = await model.register(user);
  return result;
};

export const login = async ({ user }) => {
  const result = await model.login(user);
  return result;
};

export const verifyUser = async ({ confirmationCode }) => {
  const result = await model.verifyUser(confirmationCode);
  return result;
};




