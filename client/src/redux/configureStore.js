import { combineReducers, createStore, applyMiddleware } from "redux";
import createSagaMiddleware from "@redux-saga/core";
import contactReducer from "./ducks/contact"
import authReducer from "./ducks/auth"
import alertReducer from "./ducks/alert"
import { watcherSaga } from "./sagas/rootSaga";

const reducer = combineReducers({
    contact: contactReducer,
    auth: authReducer,
    alert: alertReducer,
})

const sagaMiddleware = createSagaMiddleware();

const middleware = [sagaMiddleware];

const store = createStore(
    reducer, {}, applyMiddleware(...middleware)
);

sagaMiddleware.run(watcherSaga);

export default store;