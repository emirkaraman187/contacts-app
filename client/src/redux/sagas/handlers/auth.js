import { call, put } from "redux-saga/effects";
import { reset_alerts, set_error_msg, set_success_msg } from "../../ducks/alert";
import { set_token } from "../../ducks/auth";
import { requestLogin, requestRegister } from "../requests/auth";

const delay = (ms) => new Promise(resolve => setTimeout(resolve, ms))

export function* handleRegisterUser({ user }){
    try {
        yield call(requestRegister, user);
        yield put(set_success_msg("Veriffication link has been sent to your email address."));
        yield call(delay, 4000);
        window.location.href = '/login';
    } catch (error) {
        yield put(set_error_msg(error?.response?.data?.details?.reason));
        yield call(delay, 3000);
        yield put(reset_alerts());
    }
}

export function* handleLoginUser({ user }){
    try {
        const { data } = yield call(requestLogin, user);
        yield put(set_token(data)); 
        yield put(set_success_msg("Successfull login"));
        yield call(delay, 2000);
        window.location.href = '/';
    } catch (error) {
        yield put(set_error_msg(error?.response?.data?.details?.reason));
        yield call(delay, 3000);
        yield put(reset_alerts());
    }
}