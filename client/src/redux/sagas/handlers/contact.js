import { call, put } from "redux-saga/effects";
import { set_added_contact, set_deleted_contact, set_edited_contact, set_contacts } from "../../ducks/contact";
import { requestAddContact, requestDeleteContact, requestEditContact, requestGetContacts } from "../requests/contact";
import { reset_alerts, set_error_msg, set_success_msg } from "../../ducks/alert";
import { delete_token } from "../../ducks/auth";

const delay = (ms) => new Promise(resolve => setTimeout(resolve, ms))

export function* handleGetContacts({ idUser }){
    try {
        const { data } = yield call(requestGetContacts, idUser)
        const { contacts, invalidToken } = data;
        if(invalidToken){
            yield put(delete_token());
            window.location.href = '/login';
        }
        else{
            yield put(set_contacts(contacts));

        }
    } catch (error) {
        yield put(set_error_msg(error?.response?.data?.details?.reason));
        yield call(delay, 3000);
        yield put(reset_alerts());
    }
}

export function* handleAddContact({ contact, idUser }){
    try {
        const { data } = yield call(requestAddContact, contact, idUser);
        console.log(data);
        const { invalidToken } = data;
        if(invalidToken){
            yield put(delete_token());
            window.location.href = '/login';
        }else{
            yield put(set_added_contact(data));   
            yield put(set_success_msg("Successfully added contact"));
            yield call(delay, 3000);
            yield put(reset_alerts());
        }
    } catch (error) {
        yield put(set_error_msg(error?.response?.data?.details?.reason));
        yield call(delay, 3000);
        yield put(reset_alerts());
    }
}

export function* handleEditContact({ contact, id }){
    try {
        const { data } = yield call(requestEditContact, contact, id);
        console.log(data);
        const { invalidToken } = data;
        if(invalidToken){
            yield put(delete_token());
            window.location.href = '/login';
        }else{
            yield put(set_edited_contact(data)); 
            yield put(set_success_msg("Successfully edited contact"));
            yield call(delay, 3000);
            yield put(reset_alerts());  
        }
    } catch (error) {
        yield put(set_error_msg(error?.response?.data?.details?.reason));
        yield call(delay, 3000);
        yield put(reset_alerts());
    }
}

export function* handleDeleteContact({ id }){
    try {
        const { data } = yield call(requestDeleteContact, id);
        console.log(data);
        const { invalidToken } = data;
        if(invalidToken){
            yield put(delete_token());
            window.location.href = '/login';
        }else{
            yield put(set_deleted_contact(data));
            yield put(set_success_msg("Successfully deleted contact"));
            yield call(delay, 3000);
            yield put(reset_alerts());
        }
    } catch (error) {
        yield put(set_error_msg(error?.response?.data?.details?.reason));
        yield call(delay, 3000);
        yield put(reset_alerts());
    }
}