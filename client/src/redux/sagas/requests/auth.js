import axios from '../../../utils/axios';

export function requestRegister(user){
    return axios.request({
        method: 'post',
        url: 'auth/register',
        data: {
            user: user
        }
    })
}

export function requestLogin(user){
    return axios.request({
        method: 'post',
        url: 'auth/login',
        data: {
            user: user
        }
    })
}

