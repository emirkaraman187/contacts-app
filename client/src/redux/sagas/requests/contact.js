import axios from '../../../utils/axios';
import authHeader from '../../../utils/authHeader';

// ovo ces pozivati asinhrono
export function requestGetContacts(idUser, filters){
    return axios.request({
        method: 'get',
        url: `contact/getAllContacts/${idUser}/${filters}`,
        headers: authHeader
    })
}

export function requestAddContact(contact, idUser){
    return axios.request({
        method: 'post',
        url: `contact/createContact/${idUser}`,
        data: {
            contact: contact
        },
        headers: authHeader
    })
}

export function requestEditContact(contact, id){
    return axios.request({
        method: 'put',
        url: `contact/editContact/${id}`,
        data: {
            contact: contact,
        },
        headers: authHeader
    })
}

export function requestDeleteContact(id){
    return axios.request({
        method: 'delete',
        url: `contact/deleteContact/${id}`,
        headers: authHeader
    })
}