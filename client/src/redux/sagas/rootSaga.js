import { takeLatest } from "redux-saga/effects";
import { handleAddContact, handleDeleteContact, handleEditContact, handleGetContacts } from "./handlers/contact";
import { GET_CONTACTS, ADD_CONTACT, EDIT_CONTACT, DELETE_CONTACT } from "../ducks/contact";
import { LOGIN_USER, REGISTER_USER } from "../ducks/auth";
import { handleLoginUser, handleRegisterUser } from "./handlers/auth";

export function* watcherSaga() {
    yield takeLatest(GET_CONTACTS, handleGetContacts); 
    yield takeLatest(ADD_CONTACT, handleAddContact); 
    yield takeLatest(EDIT_CONTACT, handleEditContact); 
    yield takeLatest(DELETE_CONTACT, handleDeleteContact); 

    yield takeLatest(REGISTER_USER, handleRegisterUser); 
    yield takeLatest(LOGIN_USER, handleLoginUser); 
}