const SET_ERROR_MSG = 'SET_ERROR_MSG'
const SET_SUCCESS_MSG = 'SET_SUCCESS_MSG'
const RESET_ALERTS = 'RESET_ALERTS'

export const set_error_msg = (errorMsg) => ({
    type: SET_ERROR_MSG,
    errorMsg,
})

export const set_success_msg = (successMsg) => ({
    type: SET_SUCCESS_MSG,
    successMsg,
})

export const reset_alerts = () => ({
    type: RESET_ALERTS,
})

const initalState = {
    errorMsg: null,
    successMsg: null,
}

// eslint-disable-next-line import/no-anonymous-default-export
export default(state = initalState, action) => {
    switch(action.type){
        case SET_ERROR_MSG:
            console.log("error message: ", action.errorMsg);
            return {
                ...state,
                errorMsg: action.errorMsg,
            }
        case SET_SUCCESS_MSG:
            console.log("success message: ", action.successMsg);
            return {
                ...state,
                successMsg: action.successMsg,
            }
        case RESET_ALERTS:
            console.log("reseting alerts")
            return {
                ...state,
                successMsg: null,
                errorMsg: null,
            }
        default:
            return state;
    }
}