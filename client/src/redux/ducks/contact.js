export const GET_CONTACTS = 'GET_CONTACTS'
const SET_CONTACTS = 'SET_CONTACTS'
export const ADD_CONTACT = 'ADD_CONTACT'
const SET_ADDED_CONTACT = 'SET_ADDED_CONTACT'
export const EDIT_CONTACT = 'EDIT_CONTACT'
const SET_EDITED_CONTACT = 'SET_EDITED_CONTACT'
export const DELETE_CONTACT = 'DELETE_CONTACT'
const SET_DELETED_CONTACT = 'SET_DELETED_CONTACT'

export const get_contacts = (idUser) => ({
    type: GET_CONTACTS,
    idUser,
})

export const set_contacts = (contacts) => ({
    type: SET_CONTACTS,
    contacts,
})

export const add_contact = (contact, idUser) => ({
    type: ADD_CONTACT,
    contact,
    idUser,
})

export const set_added_contact = (contact) => ({
    type: SET_ADDED_CONTACT,
    contact,
})

export const edit_contact = (contact, id) => ({
    type: EDIT_CONTACT,
    contact,
    id,
})

export const set_edited_contact = (contact) => ({
    type: SET_EDITED_CONTACT,
    contact,
})

export const delete_contact = (id) => ({
    type: DELETE_CONTACT,
    id,
})

export const set_deleted_contact = (contact) => ({
    type: SET_DELETED_CONTACT,
    contact,
})

const initalState = {
    contacts: null,
}

// eslint-disable-next-line import/no-anonymous-default-export
export default(state = initalState, action) => {
    switch(action.type){
        case SET_CONTACTS:
            const { contacts } = action;
            return {
                ...state,
                contacts: contacts
            }
        case SET_ADDED_CONTACT:
            let { contact } = action;
            return {
                ...state,
                contacts: [
                    ...state.contacts,
                    contact
                ]
            }
        case SET_EDITED_CONTACT:
            return {
                ...state,
                contacts: state.contacts.map(item => 
                    item.id === action.contact.id ? action.contact : item
                )
            }
        case SET_DELETED_CONTACT:
            return {
                ...state,
                contacts: state.contacts.filter(item => 
                    item.id !== action.contact.id
                )
            }
        default:
            return state;
    }
}