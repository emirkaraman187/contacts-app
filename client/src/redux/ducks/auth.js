export const REGISTER_USER = 'REGISTER'
export const LOGIN_USER = 'LOGIN'
const SET_TOKEN = 'SET_TOKEN'
const DELETE_TOKEN = 'DELETE_TOKEN'

export const register_user = (user) => ({
    type: REGISTER_USER,
    user,
})

export const login_user = (user) => ({
    type: LOGIN_USER,
    user,
})

export const set_token = ({ token, email, id, firstName, lastName }) => ({
    type: SET_TOKEN,
    token,
    email,
    id,
    firstName,
    lastName,
})

export const delete_token = () => ({
    type: DELETE_TOKEN,
})

const initalState = {
    token: null,
    email: null,
    id: null,
}

// eslint-disable-next-line import/no-anonymous-default-export
export default(state = initalState, action) => {
    switch(action.type){
        case SET_TOKEN:
            localStorage.setItem('token', action.token);
            localStorage.setItem('email', action.email);
            localStorage.setItem('id', action.id);
            localStorage.setItem('firstName', action.firstName);
            localStorage.setItem('lastName', action.lastName);
            return {
                ...state,
                token: action.token,
                email: action.email,
                id: action.id,
                firstName: action.firstName,
                lastName: action.lastName,
            }
        case DELETE_TOKEN:
            localStorage.clear();
            return {
                ...state,
                token: null,
                email: null,
                id: null,
                firstName: null,
                lastName: null,
            }
        default:
            return state;
    }
}