import Snackbar from '@mui/material/Snackbar';
import Box from '@mui/material/Box';
import React from 'react'
import Alert from '@mui/material/Alert';
import { makeStyles } from '@mui/styles';

const useStyles = makeStyles((theme) => ({
    root: {
      width: '40%',
      '& > * + *': {
        marginTop: theme.spacing(2),
      },
    },
  }));

function AlertComponent({errorMsg, successMsg}) {
    const classes = useStyles();
    return (
        <Box>
            { errorMsg && !successMsg ? (
                <Snackbar
                    open = {errorMsg !== null && successMsg === null}
                    autoHideDuration={5000}
                    anchorOrigin={{ vertical: 'bottom', horizontal: 'right' }}
                    className={classes.root}
                >
                    <Alert variant="filled" severity="error">
                        {errorMsg}
                    </Alert>
                </Snackbar>
            ) : ( successMsg && !errorMsg ? (
                <Snackbar 
                    open = { errorMsg === null && successMsg !== null}
                    autoHideDuration={5000}
                    anchorOrigin={{ vertical: 'bottom', horizontal: 'right' }}
                    className={classes.root}
                >
                    <Alert variant="filled" severity="success">
                        {successMsg}
                    </Alert>
                </Snackbar>
            ) : "" )} 
        </Box>
    )
}
export default AlertComponent