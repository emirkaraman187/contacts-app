import React from 'react'
import AppBar from '@mui/material/AppBar';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import { useDispatch } from 'react-redux';
import { delete_token } from '../redux/ducks/auth';
import ContactsIcon from '@mui/icons-material/Contacts';
import { useNavigate } from 'react-router-dom';

function PageHeader() {
    const dispatch = useDispatch();
    const navigate = useNavigate();

    const handleRegister = () => {
        navigate('/register');
    }

    const handleLogin = () => {
        navigate('/login');
    }

    const handleLogout = () => {
        dispatch(delete_token());
        navigate('/login');
    }

    const firstName = localStorage.getItem('firstName') || null;
    const lastName = localStorage.getItem('lastName') || null;

    return (
        <div>
            <AppBar position="relative">
                <Toolbar>
                    <ContactsIcon sx={{ mr: 2 }} />
                    <Typography variant="h6" color="inherit" noWrap style={{width: '10%'}}>
                        CONTACT LIST
                    </Typography>
                    <Typography noWrap style={{width: '80%', color: 'yellow', fontSize: 18}}>
                        { firstName !== null && lastName !== null && `Welcome ${firstName} ${lastName}`}
                    </Typography>
                    <Box display="flex">
                        { !localStorage.getItem('token') ? (
                            <>
                                <Button onClick={() => handleRegister()} variant="outlined" style={{color: 'white'}}>
                                    REGISTER
                                </Button>
                                <Button onClick={() => handleLogin()} variant="outlined" style={{color: 'white'}}>
                                    LOGIN
                                </Button>
                            </>
                        ) : (
                            <Button onClick={() => handleLogout()} variant="outlined" style={{color: 'white'}}>
                                LOGOUT
                            </Button>
                        )}
                    </Box>
                </Toolbar>
            </AppBar>
        </div>
    )
}

export default PageHeader
