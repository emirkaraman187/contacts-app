import { useEffect } from 'react';
import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';
import Modal from '@mui/material/Modal';
import { makeStyles } from '@mui/styles';
import Grid from '@mui/material/Grid';
import OutlinedInput from '@mui/material/OutlinedInput';
import InputLabel from '@mui/material/InputLabel';
import InputAdornment from '@mui/material/InputAdornment';
import PersonIcon from '@mui/icons-material/Person';
import EmailIcon from '@mui/icons-material/Email';
import CloseIcon from '@mui/icons-material/Close';
import { useForm } from "react-hook-form";
import FormHelperText from '@mui/material/FormHelperText';
import { yupResolver } from '@hookform/resolvers/yup';
import * as yup from "yup";
import { IconButton, Tooltip } from '@mui/material';
import { useDispatch } from "react-redux";
import { add_contact, edit_contact } from '../../redux/ducks/contact';
import ContactPhoneIcon from '@mui/icons-material/ContactPhone';

const useStyles = makeStyles(theme => ({
    modal: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
    },
    paper: {
        backgroundColor: theme.palette.background.paper,
        border: '2px solid white',
        boxShadow: theme.shadows[5],
        padding: theme.spacing(2, 4, 3),
        width: '35%',
        borderRadius: '10px'
    },
}));

const ContactModalSchema = yup.object().shape({
    firstName: yup.string().max(45,'First name has maximum 45 characters')
        .required("First name is required"),
    lastName: yup.string().max(45,'Last name has maximum 45 characters')
        .required("Last name is required"),
    email: yup.string().max(45,'Email has maximum 45 characters')
        // eslint-disable-next-line no-empty-character-class
        .matches(/([-!#-'*+/-9=?A-Z^-~]+(\.[-!#-'*+/-9=?A-Z^-~]+)*|"([]!#-[^-~ \t]|(\\[\t -~]))+")@[0-9A-Za-z]([0-9A-Za-z-]{0,61}[0-9A-Za-z])?(\.[0-9A-Za-z]([0-9A-Za-z-]{0,61}[0-9A-Za-z])?)+/,
            'Email format is invalid')
        .required("Email is required"),
    phoneNumber: yup.string().max(12, 'Phone number has maximum 12 characters')
        .min(11, 'Phone number has minimum 11 characters')
        // eslint-disable-next-line no-empty-character-class
        .matches(/^(387)([0-9]+)$/, 'Phone number format is invalid, start with 387')
        .required("Phone number is required"),
  }).required();

function ContactsModal({
    contactModalOpen,
    selectedUnit,
    setContactModalOpen,
    setSelectedUnit,
    setPage,
}) {
    const classes = useStyles();
    const dispatch = useDispatch();
    const { register, handleSubmit, clearErrors, formState: { errors }, reset } = useForm({
        resolver: yupResolver(ContactModalSchema)
      });

    const submitText = selectedUnit ? "Update contact" : "Add contact"
    const titleText = selectedUnit ? "Fill the data and update contact" : "Fill the data and create new contact";

    useEffect(() => {
        clearErrors();
        reset();
    }, []);

    const handleContactModalClose = () => {
        setContactModalOpen(false);
        setSelectedUnit(null);
        reset();
        setPage(0);
    };

    const onSubmit = async data => {
        const contact = { ...data };
        if(!selectedUnit)
            dispatch(add_contact(contact, localStorage.getItem('id')));
        else{
            const id = selectedUnit?.id;
            dispatch(edit_contact(contact, id));
        }
        handleContactModalClose();
    };

    return (
        <Box>
            <Modal
                aria-labelledby="transition-modal-title"
                aria-describedby="transition-modal-description"
                className={classes.modal}
                open={contactModalOpen}
                onClose={handleContactModalClose}
            >

                    <Box className={classes.paper} overflow="auto">
                        <Box display="flex" pt={1} pb={2}>
                            <Typography 
                                align='center' 
                                variant="h5" 
                                style={{width: "95%", paddingTop: 10, fontWeight: 600}
                            }>
                                {titleText}
                            </Typography>
                            <Tooltip 
                                style={{width: "5%"}}
                                enterDelay={300}
                                placement="right"
                                arrow
                                title="Close modal"
                            >
                                <IconButton color="error" onClick={() => handleContactModalClose()}>
                                    <CloseIcon style={{width: 32, height: 32}}/>
                                </IconButton>
                            </Tooltip>
                        </Box>
                        <form onSubmit={handleSubmit(onSubmit)}>
                            <Grid container spacing={2} direction="column">
                                <Grid item xs={12}>
                                    <InputLabel htmlFor="outlined-adornment-firstname">First name</InputLabel>
                                    <OutlinedInput
                                        fullWidth
                                        id="outlined-adornment-firstname"
                                        error={!!errors?.firstName}
                                        startAdornment={
                                            <InputAdornment position="start">
                                                <PersonIcon />
                                            </InputAdornment>
                                        }
                                        {...register("firstName")}
                                        name="firstName"
                                        defaultValue={selectedUnit?.firstName}
                                        label="First name"
                                    />
                                    {errors?.firstName && (
                                        <FormHelperText>
                                            {errors?.firstName?.message}
                                        </FormHelperText>
                                    )}
                                </Grid>
                                <Grid item xs={12}>
                                    <InputLabel htmlFor="outlined-adornment-lastname">Last name</InputLabel>
                                    <OutlinedInput
                                        fullWidth
                                        id="outlined-adornment-lastname"
                                        error={!!errors?.lastName}
                                        startAdornment={
                                            <InputAdornment position="start">
                                                <PersonIcon />
                                            </InputAdornment>
                                        }
                                        {...register("lastName")}
                                        name="lastName"
                                        defaultValue={selectedUnit?.lastName}
                                        label="Last name"
                                    />
                                    {errors?.lastName && (
                                        <FormHelperText>
                                            {errors?.lastName?.message}
                                        </FormHelperText>
                                    )}
                                </Grid>
                                <Grid item xs={12}>
                                    <InputLabel htmlFor="outlined-adornment-email">Email</InputLabel>
                                    <OutlinedInput
                                        id="outlined-adornment-email"
                                        startAdornment={
                                            <InputAdornment position="start">
                                                <EmailIcon />
                                            </InputAdornment>
                                        }
                                        label="Email"
                                        fullWidth
                                        error={!!errors?.email}
                                        {...register("email")}
                                        name="email"
                                        defaultValue={selectedUnit?.email}
                                        placeholder='eg. mehmed@hotmail.ba'
                                    />
                                    {errors?.email && (
                                        <FormHelperText>
                                            {errors?.email?.message}
                                        </FormHelperText>
                                    )}
                                </Grid>
                                <Grid item xs={12}>
                                    <InputLabel htmlFor="outlined-adornment-phone-number">Phone number</InputLabel>
                                    <OutlinedInput
                                        id="outlined-adornment-phone-number"
                                        startAdornment={
                                            <InputAdornment position="start">
                                                <ContactPhoneIcon />
                                            </InputAdornment>
                                        }
                                        label="Phone number"
                                        fullWidth
                                        error={!!errors?.phoneNumber}
                                        {...register("phoneNumber")}
                                        name="phoneNumber"
                                        defaultValue={selectedUnit?.phoneNumber}
                                        placeholder='eg. 38761922871'
                                    />
                                    {errors?.phoneNumber && (
                                        <FormHelperText>
                                            {errors?.phoneNumber?.message}
                                        </FormHelperText>
                                    )}
                                </Grid>
                                <Grid item xs={12}>
                                    <Box display="flex" justifyContent="center" mt={1}>
                                        <Button type="submit" variant="contained" style={{fontWeight: 600}}>
                                            {submitText}
                                        </Button>
                                    </Box>
                                    
                                </Grid>
                            </Grid>
                        </form>
                    </Box>

            </Modal>
        </Box>
    )
}

export default ContactsModal
