import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TablePagination from '@mui/material/TablePagination';
import TableRow from '@mui/material/TableRow';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import Paper from '@mui/material/Paper';
import IconButton from '@mui/material/IconButton';
import Tooltip from '@mui/material/Tooltip';
import DeleteIcon from '@mui/icons-material/Delete';
import EditIcon from '@mui/icons-material/Edit';
import AddIcon from '@mui/icons-material/Add';
import { Divider } from '@mui/material';

const headCells = [
  {
    id: 'firstName',
    numeric: false,
    disablePadding: true,
    label: 'First name',
  },
  {
    id: 'lastName',
    numeric: false,
    disablePadding: false,
    label: 'Last name',
  },
  {
    id: 'email',
    numeric: false,
    disablePadding: false,
    label: 'Email',
  },
  {
    id: 'phoneNumber',
    numeric: false,
    disablePadding: false,
    label: 'Phone number',
  },
];

function EnhancedTableHead() {
  return (
    <TableHead>
      <TableRow>
        {headCells.map((headCell) => (
          <TableCell
            key={headCell.id}
            align={headCell.numeric ? 'right' : 'left'}
            padding='normal'
          >
            {headCell.label}
          </TableCell>
        ))}
        <TableCell />
      </TableRow>
    </TableHead>
  );
}

const EnhancedTableToolbar = ({ 
  handleContactModalOpen,
  totalCount,
}) => {
  return (
    <>
      <Toolbar
        sx={{
            pl: { sm: 2 },
            pr: { xs: 1, sm: 1 },
        }}
      >
        <Typography
            sx={{ flex: '1 1 50%' }}
            color="inherit"
            variant="h6"
            component="div"
        >
            Contact list
        </Typography>
        <Button
          variant="contained"
          color="success"
          startIcon={<AddIcon color="white"/>}
          onClick={() => {
            handleContactModalOpen();
          }}
          style={{marginRight: '10px'}}
        >
          <Box fontSize="small">Add contact</Box>
        </Button>
      </Toolbar>
      <Divider />
    </>
  );
};

export default function ContactsTable({
    contacts,
    setSelectedUnit,
    handleContactModalOpen,
    handleDeleteContactModalOpen,
    totalCount,
    page,
    setPage,
    rowsPerPage,
    setRowsPerPage,
}) {
  const handleRowsPerPageChange = ({ target: { value } }) => {
    if (value * (page + 1) >= totalCount)
      setPage(Math.floor(totalCount / value));
    setRowsPerPage(value);
  };

  const emptyRows =
    page > 0 ? Math.max(0, (1 + page) * rowsPerPage - contacts.length) : 0;

  return (
    <Box sx={{ width: '100%' }}>
      <Paper sx={{ width: '100%', mb: 2 }} elevation={8}>
        <EnhancedTableToolbar 
          handleContactModalOpen={handleContactModalOpen}
          totalCount={totalCount}
        />
        <TableContainer>
          <Table
            sx={{ minWidth: 750 }}
            aria-labelledby="tableTitle"
            size='medium'
          >
            <EnhancedTableHead />
            <TableBody>
              {totalCount > 0 ? contacts.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                .map((row) => {
                  return (
                    <TableRow
                      hover
                      role="checkbox"
                      tabIndex={-1}
                      key={row.id}
                    >
                      <TableCell
                        component="th"
                        scope="row"
                        padding="normal"
                      >
                        {row.firstName}
                      </TableCell>
                      <TableCell align="left">{row.lastName}</TableCell>
                      <TableCell align="left">{row.email}</TableCell>
                      <TableCell align="left">{row.phoneNumber}</TableCell>
                      <TableCell align="right">
                        <Tooltip title="Edit Contact" enterDelay={300} arrow>
                            <IconButton onClick={() => { 
                                setSelectedUnit(row);
                                handleContactModalOpen();
                            }}>
                                <EditIcon color="primary"/>
                            </IconButton>
                        </Tooltip>
                        <Tooltip title="Delete Contact" enterDelay={300} arrow>
                            <IconButton onClick={() => { 
                                setSelectedUnit(row);
                                handleDeleteContactModalOpen();
                            }}>
                                <DeleteIcon color="error"/>
                            </IconButton>
                        </Tooltip>
                      </TableCell>
                    </TableRow>
                  );
                }) : (
                  <Box mt={2} ml={2}>
                    <Typography variant="h5">You are currently having zero contacts in a contact list</Typography>
                  </Box>
                )}
              {emptyRows > 0 && (
                <TableRow
                  style={{
                    height: 53 * emptyRows,
                  }}
                >
                  <TableCell colSpan={6} />
                </TableRow>
              )}
            </TableBody>
          </Table>
        </TableContainer>
        <TablePagination
          rowsPerPageOptions={[5, 10, 25]}
          width="100%"
          component="div"
          count={totalCount}
          rowsPerPage={rowsPerPage}
          page={page}
          onPageChange={(_, newPage) => setPage(newPage)}
          onRowsPerPageChange={handleRowsPerPageChange}
        />
      </Paper>
    </Box>
  );
}