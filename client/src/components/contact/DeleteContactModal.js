import * as React from 'react';
import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import Modal from '@mui/material/Modal';
import { makeStyles } from '@mui/styles';
import Grid from '@mui/material/Grid';
import CloseIcon from '@mui/icons-material/Close';
import { Card, CardContent, CardHeader, Divider, IconButton, Tooltip } from '@mui/material';
import { useDispatch } from "react-redux";
import { delete_contact } from '../../redux/ducks/contact';

const useStyles = makeStyles(theme => ({
    modal: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
    },
    paper: {
        backgroundColor: theme.palette.background.paper,
        border: '2px solid white',
        boxShadow: theme.shadows[5],
        width: '30%',
        borderRadius: '8px'
    },
}));

function DeleteContactModal({
    deleteContactModalOpen,
    selectedUnit,
    setDeleteContactModalOpen,
    setSelectedUnit,
    setPage,
}) {
    const classes = useStyles();
    const dispatch = useDispatch();

    const handleDeletContactModalClose = () => {
        setDeleteContactModalOpen(false);
        setSelectedUnit(null);
        setPage(0);
    };

    const deleteContact = async () => {
        dispatch(delete_contact(selectedUnit?.id));
        handleDeletContactModalClose();
    } 

    return (
        <Box>
            <Modal
                aria-labelledby="transition-modal-title"
                aria-describedby="transition-modal-description"
                className={classes.modal}
                open={deleteContactModalOpen}
                onClose={handleDeletContactModalClose}
            >
                <Box className={classes.paper} overflow="auto">
                    <Card>
                        <CardHeader
                            action={
                                <Tooltip 
                                    enterDelay={300}
                                    placement="right"
                                    arrow
                                    title="Close modal"
                                >
                                    <IconButton color="error" onClick={() => handleDeletContactModalClose()}>
                                        <CloseIcon style={{width: 28, height: 28}}/>
                                    </IconButton>
                                </Tooltip>
                            }
                            title={`Do you want to delete: ${selectedUnit?.firstName} ${selectedUnit?.lastName}`}
                            titleTypographyProps={{ variant: 'h6' }}
                        />
                        <Divider />
                        <CardContent>
                            <Grid container spacing={2} direction="row" style={{marginTop: 1}}>
                                <Grid item xs={4} />
                                <Grid item xs={2}>
                                    <Box display="flex" justifyContent="center">
                                        <Button 
                                            onClick={() => deleteContact()} 
                                            variant="contained" 
                                            color="success"
                                        >
                                            YES
                                        </Button>
                                    </Box>
                                </Grid>
                                <Grid item xs={2}>
                                    <Box display="flex" justifyContent="center" mb={1}>
                                        <Button 
                                            onClick={() => handleDeletContactModalClose()} 
                                            variant="contained" 
                                            color="error"
                                        >
                                            NO
                                        </Button>
                                    </Box>
                                </Grid>
                            </Grid>
                        </CardContent>
                    </Card>
                </Box>
            </Modal>
        </Box>
    )
}

export default DeleteContactModal
