import * as React from 'react';
import CssBaseline from '@mui/material/CssBaseline';
import { createTheme, ThemeProvider } from '@mui/material/styles';
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';
import Homepage from './views/Homepage';
import Login from './views/Login';
import Register from './views/Register';
import PageHeader from './components/PageHeader';

function MainView() {
  const theme = createTheme();

  return (
    <ThemeProvider theme={theme}>
      <CssBaseline />
        <Router>
          <PageHeader/>
          <Routes>
            <Route path="/" exact element={<Homepage />} />
            <Route path="/login" exact element={<Login />} />
            <Route path="/register" exact element={<Register />} />
            <Route path="*" element={<Homepage />} />
          </Routes>
        </Router>
    </ThemeProvider>
  );
}

export default MainView;
