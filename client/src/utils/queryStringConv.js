const queryStringConv = (filters) => {
    return Object.keys(filters).map(key => key + '=' + filters[key]).join('&');
}

export default queryStringConv;
