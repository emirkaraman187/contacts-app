const authHeader =  {
    Authorization: `Bearer ${localStorage.getItem('token')}`
}

export default authHeader;