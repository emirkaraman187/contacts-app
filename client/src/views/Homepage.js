import { Box, Typography } from '@mui/material'
import React, { useEffect, useState } from 'react'
import ContactsTable from '../components/contact/ContactsTable';
import { useSelector, useDispatch } from "react-redux";
import { get_contacts } from '../redux/ducks/contact';
import CircularProgress from '@mui/material/CircularProgress';
import ContactsModal from '../components/contact/ContactsModal';
import DeleteContactModal from '../components/contact/DeleteContactModal';
import { useNavigate } from 'react-router-dom';
import AlertComponent from '../components/AlertComponent';

function Homepage() {
    const dispatch = useDispatch();
    const navigate = useNavigate();
    const [selectedUnit, setSelectedUnit] = useState(null)
    const [contactModalOpen, setContactModalOpen] = useState(false);
    const [deleteContactModalOpen, setDeleteContactModalOpen] = useState(false);
    const [page, setPage] = useState(0);
    const [rowsPerPage, setRowsPerPage] = useState(5);

    const handleDeleteContactModalOpen = () => {
        setDeleteContactModalOpen(true);
    };

    const handleContactModalOpen = () => {
        setContactModalOpen(true);
    };

    const getContacts = async () => {
        if(localStorage.getItem('token') && localStorage.getItem('id'))
            dispatch(get_contacts(localStorage.getItem('id')));     
        else
            navigate('/login');
    }

    useEffect(() => {
        getContacts();
        setPage(0);
    // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [dispatch])

    const contacts = useSelector(state => state.contact.contacts);
    const errorMsg = useSelector(state => state.alert.errorMsg);
    const successMsg = useSelector(state => state.alert.successMsg);

    return (
        <Box mt={5} ml={5} mr={5}>
            <AlertComponent errorMsg={errorMsg} successMsg={successMsg}/>
            { contacts ? (
                <>
                    <Typography align="center" variant="h5" style={{marginBottom: 30}}>
                        Contacts
                    </Typography>
                    <ContactsTable 
                        contacts={contacts}
                        setSelectedUnit={setSelectedUnit}
                        handleContactModalOpen={handleContactModalOpen}
                        handleDeleteContactModalOpen={handleDeleteContactModalOpen}
                        totalCount={contacts?.length}
                        page={page}
                        setPage={setPage}
                        rowsPerPage={rowsPerPage}
                        setRowsPerPage={setRowsPerPage}
                    />
                    <ContactsModal 
                        contactModalOpen={contactModalOpen}
                        selectedUnit={selectedUnit}
                        setContactModalOpen={setContactModalOpen}
                        setSelectedUnit={setSelectedUnit}
                        setPage={setPage}
                    />
                    <DeleteContactModal 
                        deleteContactModalOpen={deleteContactModalOpen}
                        selectedUnit={selectedUnit}
                        setDeleteContactModalOpen={setDeleteContactModalOpen}
                        setSelectedUnit={setSelectedUnit}
                        setPage={setPage}
                    />
                </>
            ) : (
                <Box display='flex' flexDirection="column" alignItems="center" justifyContent="center" height={150}>
                    <CircularProgress />
                    <Typography align="center" variant="h6" style={{marginTop: 5}}>
                        Loading
                    </Typography>
                </Box>
            )}
        </Box>
    )
}

export default Homepage
