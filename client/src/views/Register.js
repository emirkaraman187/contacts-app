import { useEffect } from 'react';
import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import Grid from '@mui/material/Grid';
import OutlinedInput from '@mui/material/OutlinedInput';
import InputLabel from '@mui/material/InputLabel';
import InputAdornment from '@mui/material/InputAdornment';
import PersonIcon from '@mui/icons-material/Person';
import EmailIcon from '@mui/icons-material/Email';
import LockIcon from '@mui/icons-material/Lock';
import Divider from '@mui/material/Divider';
import Card from '@mui/material/Card';
import CardHeader from '@mui/material/CardHeader';
import CardContent from '@mui/material/CardContent';
import { useForm } from "react-hook-form";
import FormHelperText from '@mui/material/FormHelperText';
import { yupResolver } from '@hookform/resolvers/yup';
import * as yup from "yup";
import { useDispatch, useSelector } from "react-redux";
import {  register_user } from '../redux/ducks/auth';
import Link from '@mui/material/Link';
import AlertComponent from '../components/AlertComponent';
import { useNavigate } from 'react-router-dom';

const RegisterSchema = yup.object().shape({
    firstName: yup.string().max(45,'First name has maximum 45 characters')
        .required("First name is required"),
    lastName: yup.string().max(45,'Last name has maximum 45 characters')
        .required("Last name is required"),
    password: yup.string().max(20,'Password has maximum 20 characters')
        .min(6,'Password has minimum 6 characters')
        .required("Password is required"),
    email: yup.string().max(45,'Email has maximum 45 characters')
        // eslint-disable-next-line no-empty-character-class
        .matches(/([-!#-'*+/-9=?A-Z^-~]+(\.[-!#-'*+/-9=?A-Z^-~]+)*|"([]!#-[^-~ \t]|(\\[\t -~]))+")@[0-9A-Za-z]([0-9A-Za-z-]{0,61}[0-9A-Za-z])?(\.[0-9A-Za-z]([0-9A-Za-z-]{0,61}[0-9A-Za-z])?)+/,
            'Email format is invalid')
        .required("Email is required"),
  }).required();

function Register() {
    const navigate = useNavigate();
    const dispatch = useDispatch();
    const { register, handleSubmit, clearErrors, formState: { errors }, reset } = useForm({
        resolver: yupResolver(RegisterSchema)
      });

    useEffect(() => {
        if(localStorage.getItem('token'))
            navigate('/')
        clearErrors();
        reset();
    }, []);

    const onSubmit = async data => {
        const user = { ...data };
        dispatch(register_user(user)); 
    };

    const errorMsg = useSelector(state => state.alert.errorMsg);
    const successMsg = useSelector(state => state.alert.successMsg);

    return (
        <Box
            display="flex"
            alignContent="center"
            justifyContent="center"
            mt={5}
        >
            <AlertComponent errorMsg={errorMsg} successMsg={successMsg}/>
            <Card elevation={4} style={{ height: '100%', width: "40%", marginTop: 20 }}>
              <CardHeader
                title="REGISTER"
                titleTypographyProps={{ variant: 'h6', align: 'center' }}
              />
              <Divider />
              <CardContent>
                    <form onSubmit={handleSubmit(onSubmit)}>
                        <Grid container spacing={2} direction="column">
                            <Grid item xs={12}>
                                <InputLabel htmlFor="outlined-adornment-firstname">First name</InputLabel>
                                <OutlinedInput
                                    fullWidth
                                    id="outlined-adornment-firstname"
                                    error={!!errors?.firstName}
                                    startAdornment={
                                        <InputAdornment position="start">
                                            <PersonIcon />
                                        </InputAdornment>
                                    }
                                    {...register("firstName")}
                                    name="firstName"
                                    label="First name"
                                />
                                {errors?.firstName && (
                                    <FormHelperText>
                                        {errors?.firstName?.message}
                                    </FormHelperText>
                                )}
                            </Grid>
                            <Grid item xs={12}>
                                <InputLabel htmlFor="outlined-adornment-lastname">Last name</InputLabel>
                                <OutlinedInput
                                    fullWidth
                                    id="outlined-adornment-lastname"
                                    error={!!errors?.lastName}
                                    startAdornment={
                                        <InputAdornment position="start">
                                            <PersonIcon />
                                        </InputAdornment>
                                    }
                                    {...register("lastName")}
                                    name="lastName"
                                    label="Last name"
                                />
                                {errors?.lastName && (
                                    <FormHelperText>
                                        {errors?.lastName?.message}
                                    </FormHelperText>
                                )}
                            </Grid>
                            <Grid item xs={12}>
                                <InputLabel htmlFor="outlined-adornment-email">Email</InputLabel>
                                <OutlinedInput
                                    id="outlined-adornment-email"
                                    startAdornment={
                                        <InputAdornment position="start">
                                            <EmailIcon />
                                        </InputAdornment>
                                    }
                                    label="Email"
                                    fullWidth
                                    error={!!errors?.email}
                                    {...register("email")}
                                    name="email"
                                />
                                {errors?.email && (
                                    <FormHelperText>
                                        {errors?.email?.message}
                                    </FormHelperText>
                                )}
                            </Grid>
                            <Grid item xs={12}>
                                <InputLabel htmlFor="outlined-adornment-password">Password</InputLabel>
                                <OutlinedInput
                                    id="outlined-adornment-password"
                                    type="password"
                                    startAdornment={
                                        <InputAdornment position="start">
                                            <LockIcon />
                                        </InputAdornment>
                                    }
                                    label="Password"
                                    fullWidth
                                    error={!!errors?.password}
                                    {...register("password")}
                                    name="password"
                                />
                                {errors?.password && (
                                    <FormHelperText>
                                        {errors?.password?.message}
                                    </FormHelperText>
                                )}
                            </Grid>
                            <Grid item xs={12}>
                                <Box display="flex" justifyContent="center" pt={2}>
                                    <Button type="submit" variant="contained" style={{width: '20%'}}>
                                        SUBMIT
                                    </Button>
                                </Box>
                            </Grid>
                            <Grid item xs={12}>
                                <Box display="flex" justifyContent="center">
                                    <Link href="/login" underline="none" style={{fontWeight:'700'}}>
                                        Already have an account? Login
                                    </Link>
                                </Box>
                            </Grid>
                        </Grid>
                    </form>
                </CardContent>
            </Card>
        </Box>
    )
}

export default Register
